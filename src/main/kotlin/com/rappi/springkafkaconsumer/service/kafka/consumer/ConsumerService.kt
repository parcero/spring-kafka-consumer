package com.rappi.springkafkaconsumer.service.kafka.consumer

import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Service
import java.util.concurrent.CountDownLatch

@Service
class ConsumerService {
    private val logger = LoggerFactory.getLogger(ConsumerService::class.java)
    val latch = CountDownLatch(1)


    @KafkaListener(topics = ["\${kafka.topic.transactions}"])
    fun receiveTransactions(@Payload payload: String) {
        logger.debug("received payload='$payload'")

        latch.countDown()
    }
}
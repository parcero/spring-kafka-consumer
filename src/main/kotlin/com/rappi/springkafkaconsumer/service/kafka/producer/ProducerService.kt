package com.rappi.springkafkaconsumer.service.kafka.producer

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service

@Service
class ProducerService {
    private val logger = LoggerFactory.getLogger(ProducerService::class.java)

    @Autowired
    lateinit var kafkaTemplate: KafkaTemplate<String, String>

    fun send(topic: String, payload: String) {
        logger.debug("sending payload=$payload to topic=$topic")

        kafkaTemplate.send(topic, payload)
    }
}
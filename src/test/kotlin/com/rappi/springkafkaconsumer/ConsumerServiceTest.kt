package com.rappi.springkafkaconsumer

import com.rappi.springkafkaconsumer.service.kafka.consumer.ConsumerService
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.config.KafkaListenerEndpointRegistry
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.test.rule.KafkaEmbedded
import org.springframework.kafka.test.utils.ContainerTestUtils
import org.springframework.kafka.test.utils.KafkaTestUtils
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit4.SpringRunner
import java.util.concurrent.TimeUnit

@RunWith(SpringRunner::class)
@SpringBootTest
@DirtiesContext
class ConsumerServiceTest {
    private val logger = LoggerFactory.getLogger(ConsumerServiceTest::class.java)

    companion object {
        const val transactionsTopic = "transactions.t"

        @ClassRule
        @JvmField
        val embeddedKafka = KafkaEmbedded(1, true, transactionsTopic)
    }

    @Autowired
    lateinit var consumer: ConsumerService
    @Autowired
    lateinit var kafkaListenerEndpointRegistry: KafkaListenerEndpointRegistry
    lateinit var kafkaTemplate: KafkaTemplate<String, String>

    @Before
    fun setUp() {
        // set up the Kafka producer properties
        val senderProperties = KafkaTestUtils.senderProps(embeddedKafka.brokersAsString)

        // create a Kafka producer factory
        val producerFactory = DefaultKafkaProducerFactory<String, String>(senderProperties)

        // create a Kafka template
        kafkaTemplate = KafkaTemplate(producerFactory)

        // set the default topic to send to
        kafkaTemplate.defaultTopic = transactionsTopic

        // wait until the partitions are assigned
        for (messageListenerContainer in kafkaListenerEndpointRegistry.listenerContainers) {
            ContainerTestUtils.waitForAssignment(messageListenerContainer, embeddedKafka.partitionsPerTopic)
        }
    }

    @Test
    fun testReceiveTransactions() {
        // send the message
        val payload = "transaction payload..."

        kafkaTemplate.sendDefault(payload)
        logger.debug("sender sent transaction payload='$payload'")

        consumer.latch.await(10000, TimeUnit.MILLISECONDS)
        assertThat(consumer.latch.count).isEqualTo(0L)
    }
}


package com.rappi.springkafkaconsumer

import com.rappi.springkafkaconsumer.service.kafka.consumer.ConsumerService
import com.rappi.springkafkaconsumer.service.kafka.producer.ProducerService
import org.assertj.core.api.Assertions
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.test.rule.KafkaEmbedded
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit4.SpringRunner
import java.util.concurrent.TimeUnit

@RunWith(SpringRunner::class)
@SpringBootTest
@DirtiesContext
class ApplicationTests {
    companion object {
        const val TOPIC = "com.rappi.topic.transactions"

        @ClassRule
        @JvmField
        val embeddedKafka = KafkaEmbedded(1, true, TOPIC)
    }

    @Autowired
    lateinit var consumer: ConsumerService
    @Autowired
    lateinit var producer: ProducerService

    @Test
    fun testReceive() {
        producer.send(TOPIC, "test payload")

        consumer.latch.await(10000, TimeUnit.MILLISECONDS)
        Assertions.assertThat(consumer.latch.count).isEqualTo(0L)
    }
}